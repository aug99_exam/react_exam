import ProjCard from '../components/ProjCard';


import {Container, Row, Col, Card, Button, Badge, Accordion} from 'react-bootstrap';


export default function Home ()
{
	

	return (
		<>
		<Container fluid className="justify-content-center py-5">
			<Row>
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>Starlink 1 (v0.9) <Badge bg="success">
				        	        Success
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				          Starlink 1 - Space Ex
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>GPS SV05 <Badge bg="info">
				        	        Upcoming
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				         
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">In a year</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>GTO-2 <Badge bg="info">
				        	        Upcoming
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				       
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>CRS-21 <Badge bg="info">
				        	        Upcoming
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>SXM-8 <Badge bg="info">
				        	        Upcoming
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>CRS-18 <Badge bg="success">
				        	        Success
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				          CRS-18 - Space Ex
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">SpaceX's 18th Commercial Resupply Services mission out of a total of 20 such contracted flights for NASA, this launch will deliver essential supplies to the International Space Station using the reusable Dragon 1 cargo spacecraft. The external payload for this mission is International Docking Adapter 3, replacing IDA-1 lost in SpaceX's CRS-7 launch failure. This mission will launch from SLC-40 at Cape Canaveral AFS on a Falcon 9, and the first-stage booster is expected to land back at CCAFS LZ-1.</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>Trailblazer <Badge bg="danger">
				        	        failed
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				          Trailblazer Project
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>

				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>DemoSat <Badge bg="danger">
				        	        failed
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				          Demosat Project
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>
				
				<Card className= "mb-3">
				    <Card.Body>
				        <Card.Title>FalconSat <Badge bg="danger">
				        	        failed
				        	</Badge></Card.Title>
				        	
				        <Card.Text>
				          FalconSat Project
				        </Card.Text>
				      </Card.Body>

				      <Accordion defaultActiveKey="0"  className= "mb-3">
				          <Accordion.Item>
				          <Accordion.Header>
				         <Button variant="primary">View</Button>
				          </Accordion.Header>
				          				   
				          	<Accordion.Body>
				          	<div className="align-items-left">Sample Text Here</div>           			
			             	</Accordion.Body>
			          </Accordion.Item>
			      </Accordion>
				</Card>



			</Row>
		</Container>
              
		</>
		)
}
