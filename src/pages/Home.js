import Banner from '../components/Banner';


import {Container, Row, Col} from 'react-bootstrap';


export default function Home ()
{
	const data = 
	{
		title: "Space Ex",
		content: "Info of Various space Exploration Projects",
		destination: "/search",
		label: "Search"
	}

	return (
		<>
		<Container fluid className="justify-content-center py-5">
			<Row>
				<Banner dataProp={data} />
				
			</Row>
		</Container>
              
		</>
		)
}
