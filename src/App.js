import logo from './logo.svg';
import './App.css';

// COMPONENTS
import AppNavBar from './components/AppNavBar';

//PACKAGES
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import {Container, Row, Col} from 'react-bootstrap';

//PAGES
import Home from './pages/Home';
import Search from './pages/Search';
  

function App() {
  return (
    

    <Router>
      <>
      <AppNavBar />
        
      <Container>
        <Switch>
          <Route exact path = "/" component = {Home}/>
          <Route exact path = "/search" component = {Search}/>
         
        </Switch>
      </Container>


      </>
    </Router> 
  );
}

export default App;


