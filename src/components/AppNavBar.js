// IMPORTS
import {Button, InputGroup, Badge} from 'react-bootstrap';

import {Container, Row, Col, Form, Nav, Navbar, NavDropdown} from 'react-bootstrap';




export default function AppNavBar() 
{
	 return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="/">Aug 99 React Exam</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="#link">Link</Nav.Link>
            <NavDropdown title="Space Ex" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action 1</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Action 2
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Action 3</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                About Us
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}