import {Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'


export default function ProjCard ({cardProp})
{
	
	const {name, description} = cardProp;
	

	return (
			<Col>
				<Card className = "projCard my-2">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle className = "mt-3"> Description: </Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						
       					 <Link className= "btn btn-primary" to= {`search`} >Details</Link>
					</Card.Body>
					
					
				</Card>

			</Col>
		)
}